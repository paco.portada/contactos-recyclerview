git initpackage com.example.contactos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Contact> contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // ...
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Lookup the recyclerview in activity layout
        RecyclerView rvContacts = (RecyclerView) findViewById(R.id.rvContacts);

        // Setup button to append additional contacts.
        Button addMoreButton = (Button) findViewById(R.id.button);

        // Initialize contacts
        contacts = Contact.createContactsList(10);
        // Create adapter passing in the sample user data
        ContactsAdapter adapter = new ContactsAdapter(contacts);
        // Attach the adapter to the recyclerview to populate items
        rvContacts.setAdapter(adapter);
        // Set layout manager to position the items
        rvContacts.setLayoutManager(new LinearLayoutManager(this));
        // That's all!

        adapter.setOnItemClickListener(new ContactsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String name = contacts.get(position).getName();
                Toast.makeText(MainActivity.this, name + " was clicked!", Toast.LENGTH_SHORT).show();
                //mostrarMensaje(name);
            }
        });


        addMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // record this value before making any changes to the existing list
                int curSize = adapter.getItemCount();
                // Add a new contact
                // contacts.add(0, new Contact("Barney", true));
                contacts.add(curSize, new Contact("Barney", true));
                // Notify the adapter that an item was inserted at position 0
                adapter.notifyItemInserted(curSize);
            }
        });
    }

    private void mostrarMensaje(String name) {
        Toast.makeText(this, name + " was clicked!", Toast.LENGTH_SHORT).show();
    }
}